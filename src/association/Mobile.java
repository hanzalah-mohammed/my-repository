package association;

public class Mobile {

    Whatsapp refWhatsapp;

    public Whatsapp getRefWhatsapp() {
        return refWhatsapp;
    }

    public void setRefWhatsapp(Whatsapp refWhatsapp) {
        this.refWhatsapp = refWhatsapp;
    }
}
