// Concept of association

package association;

public class ExampleMain {
    public static void main(String[] args) {

        Chat refChat = new Chat();
        refChat.setChatMsg("Hanzalah is awesome!!!");

        Whatsapp refWhatsapp = new Whatsapp();
        refWhatsapp.setRefChat(refChat);

        Mobile refMobile = new Mobile();
        refMobile.setRefWhatsapp(refWhatsapp);

        System.out.println(refMobile.getRefWhatsapp().getRefChat().getChatMsg());
    }
}
