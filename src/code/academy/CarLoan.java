package code.academy;

public class CarLoan {
    public static void main(String[] args) {

        int carLoan = 10000;
        int loanLength = 3; //Loan length of 3 years
        int interestRate = 5; //Interest rate of 5%
        int downPayment = 3000;

        if (loanLength <= 0 || interestRate <= 0) {
            System.out.println("Error! You must take out a valid car loan");
        } else if (downPayment >= carLoan){
            System.out.println("The car can be paid in full.");
        } else { int remainingBalance = carLoan - downPayment;
            int totalMonth = loanLength * 12;
            int monthlyBalance = remainingBalance / totalMonth;
            int interest = (monthlyBalance * interestRate) / 100;
            int monthlyPayment = monthlyBalance + interest;
            System.out.println(monthlyPayment);
        }
    }
}