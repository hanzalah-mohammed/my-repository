package code.academy;

public class RandomNumber {

    // Math.random() will always return random float number
    // use (int) type casting to change double to int (whole number)

    static void getRandomNumber(int minValue, int maxValue){
        int number = (int)(Math.random() * (maxValue - minValue)) + minValue; // The formula to get random number between min and max
        System.out.println(number);
    }

    public static void main(String[] args) {
        getRandomNumber(1, 20);
        getRandomNumber(1, 200);
        getRandomNumber(2, 20);
        getRandomNumber(1, 20);
        getRandomNumber(1, 20);
    }
}
