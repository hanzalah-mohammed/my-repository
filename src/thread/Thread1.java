package thread;

// Create thread 1 using extends Thread class
class DrawImage extends Thread{
    @Override
    public void run() {
        System.out.println("Drawing image....");
    }
}

// Create thread 2 using implements runnable interface
class PlayMusic implements Runnable {
    @Override
    public void run() {
        System.out.println("Playing music....");
    }
}

public class Thread1 {
    public static void main(String[] args) {

        // If the class extends the Thread class,
        DrawImage refDraw = new DrawImage(); //  the thread can be run by creating an instance of the class
        refDraw.start(); // and call its start() method

        // If the class implements the Runnable interface,
        PlayMusic refPLay = new PlayMusic();
        Thread newThread = new Thread(refPLay); // the thread can be run by passing an instance of the class to a Thread object's constructor
        newThread.start(); // and then calling the thread's start() method
    }
}
