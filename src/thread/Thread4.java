package thread;

import java.util.concurrent.*;

class Task1 extends Thread{

    @Override
    public void run() {
        System.out.println("This thread implements Runnable interface");
    }
}

class Task2 implements Runnable {

    @Override
    public void run(){
        System.out.println("This thread extends Thread class");
    }
}

class Task3 implements Callable<String> {

    @Override
    public String call() {
        return "This thread implements Callable interface";
    }
}

public class Thread4 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        Task1 ref1 = new Task1();
        Task2 ref2 = new Task2();
        Task3 ref3 = new Task3();

        ref1.start(); // extends Thread can use .start() directly

        Thread threadForRef2 = new Thread(ref2); // implements Runnable must be passed in Thread object
        threadForRef2.start(); // use the created thread object to start

        ExecutorService refExecutor = Executors.newSingleThreadExecutor();
        Future<String> future = refExecutor.submit(ref3);
        System.out.println(future.get());

        refExecutor.shutdown();
    }


}
