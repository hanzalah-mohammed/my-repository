package thread;

class MyClass extends Thread {

    @Override
    public void run() {
        for(int i = 0; i < 4; i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Loop " + (i+1) + ": " + "Thread ID " + Thread.currentThread().getName());
        }
    }
}

public class Thread2 {
    public static void main(String[] args) {

        MyClass obj1 = new MyClass();
        MyClass obj2 = new MyClass();
        MyClass obj3 = new MyClass();
        MyClass obj4 = new MyClass();
        MyClass obj5 = new MyClass();
        MyClass obj6 = new MyClass();

        // The ID of each thread will be different and run in parallel
        // the output will differ each loop
        obj1.start();
        obj2.start();
        obj3.start();

        //if you use run() method, the ID will always be 1
        obj4.run();
        obj5.run(); // this one will wait obj4 to finish

    }
}
