package thread;

class NewThread extends Thread{

    private final String name;

    NewThread(String name){
        this.name = name;
        System.out.println(name + " has been created");
    }

    @Override
    public void run() {
        System.out.println(name + " is running");
    }
}

public class Thread3 {
    public static void main(String[] args) {

        // Object created will be in sequence
        System.out.println("\n Object created will all run in sequence.");
        System.out.println("----------------------");
        NewThread T1 = new NewThread("Thread 1");
        NewThread T2 = new NewThread("Thread 2");
        NewThread T3 = new NewThread("Thread 3");
        NewThread T4 = new NewThread("Thread 4");

        // .start() method will run in parallel
        // the sequence of execution can be varied each time
        System.out.println("\n The execution can be varied each time as all run in parallel.");
        System.out.println("----------------------");
        T1.start();
        T2.start();
        T3.start();
        T4.start();

    }
}
