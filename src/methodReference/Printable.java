package methodReference;

@FunctionalInterface
public interface Printable {
    void print();
}
