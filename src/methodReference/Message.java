package methodReference;

// 3 ways to use method reference - constructor, static, and instance method.
// 1 reference interface can only accept 1 method reference.
// Interface must be a functional interface

public class Message {

    // Constructor
    public Message() {
        System.out.println("This is from constructor.");
    }

    // Static method
    public static void staticMethod(){
        System.out.println("This is from static method.");
    }

    // Instance method
    public void instanceMethod(){
        System.out.println("This is from instance method.");
    }

    // main method
    public static void main(String[] args) {

        // calling constructor - use 'new' keyword
        // syntax -> ClassName::new
        Printable ref1 = Message::new;
        ref1.print();

        // calling static method
        // syntax -> ContainingClass::staticMethodName
        Printable ref2 = Message::staticMethod;
        ref2.print();

        // calling instance method
        // syntax -> containingObject::instanceMethodName
        Message refMsg = new Message();
        Printable ref3 = refMsg::instanceMethod;
        ref3.print();
    }
}
