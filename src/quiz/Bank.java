package quiz;

public class Bank {

    private String customerName;
    public int bankBalance;

    public Bank() {
        System.out.println("Creating bank account....");
    }

    public Bank(String name) {
        this();
        customerName = name;
        System.out.println("Setting bank name: " + name);
    }

    public Bank(String name, int bankBalance) {
        this(name);
        this.bankBalance = bankBalance;
        System.out.println("Setting bank balance of $" + bankBalance + ".");
    }

    @Override
    public String toString() {
        return "This is " + customerName + "'s bank account with cash balance of $" + bankBalance + ".";
    }

    public static void main(String[] subMain) {
        Bank lisaAcc = new Bank("Lisa", 5321);
        System.out.println(lisaAcc);
        Bank sallyAcc = new Bank("Sally");
        System.out.println(sallyAcc);
    }
}
