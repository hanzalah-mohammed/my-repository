package day7;

class Driver1 {
    private static Driver1 refDriver = null;

    private Driver1(){
        System.out.println("Calling Driver constructor..");
    }

    public static Driver1 getmethod(){
        if(refDriver == null) {
            refDriver = new Driver1();
        }
        return refDriver;
    }
}

public class Driver {
    public static void main(String[] args) {
        Driver1.getmethod();
    }
}
