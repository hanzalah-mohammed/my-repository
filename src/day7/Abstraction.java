package day7;

abstract class Kiosk {

    void paybill1(){  //concrete method which has a body

    }

    abstract void paybill2(); //abstract method without having any body (implementation)
}

class Starhub extends Kiosk {
    @Override
    void paybill2() {
        System.out.println("Pay bill...");
    }


}

public class Abstraction {
    public static void main(String[] args) {
        System.out.println("Welcome to Squad 1");
    }
}
