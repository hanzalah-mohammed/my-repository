package threadExample;

// Let's first create Data class that consists of the data packet that will be sent from Sender to Receiver.
// We'll use wait() and notifyAll() to set up synchronization between them:

public class Data {
    private String packet;

    // True if receiver should wait
    // False if sender should wait
    private boolean transferInProcess = true;

    public synchronized void send(String packet) {
        while (!transferInProcess) {
            try {
                wait(); // thread will wait if transfer is not in process
            } catch (InterruptedException e)  {
                Thread.currentThread().interrupt();
                System.err.println("Thread interrupted" + e);
            }
        }
        transferInProcess = false;

        this.packet = packet;
        notifyAll(); // notify the receiver to execute
    }

    public synchronized String receive() {
        while (transferInProcess) {
            try {
                wait(); // Thread will wait if transfer is in process
            } catch (InterruptedException e)  {
                Thread.currentThread().interrupt();
                System.err.println("Thread interrupted" + e);
            }
        }
        transferInProcess = true;

        notifyAll(); // notify the sender to execute
        return packet;
    }
}
