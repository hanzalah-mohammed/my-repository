package threadExample;

import java.util.concurrent.ThreadLocalRandom;

public class Sender implements Runnable {
    private final Data data;

    // standard constructors
    public Sender(Data data) {
        this.data = data;
    }

    @Override
    public void run() {
        String[] packets = {
                "First packet",
                "Second packet",
                "Third packet",
                "Fourth packet",
                "Fifth packet",
                "End"
        };

        for (String packet : packets) {
            if (!packet.equals("End")){
                System.out.println("Sending the " + packet + "...");
            }
            data.send(packet);

            // Thread.sleep() to mimic heavy server-side processing
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 5000));
            } catch (InterruptedException e)  {
                Thread.currentThread().interrupt();
                System.err.println("Thread interrupted" + e);
            }
        }
    }
}