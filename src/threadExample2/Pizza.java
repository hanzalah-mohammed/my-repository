package threadExample2;

public class Pizza {
    boolean pizzaOrdered = false;
    boolean pizzaArrived = false;
    String status = "empty";

    public synchronized void eatPizza() throws InterruptedException {
        while (!pizzaOrdered){
            wait();
        }
        Thread.sleep(1000);
        while (!pizzaArrived){
            System.out.println("Waiting for the pizza....");
            wait();
        }
        System.out.println("Yayyyy pizza is here!!");
        for(int i = 0; i < 3; i++){
            Thread.sleep(1000);
            System.out.println("yumyum..");
        }
        this.status = "full";
        Thread.sleep(3000);
        notifyAll();
    }

    public synchronized void orderPizza() throws InterruptedException {
        Thread.sleep(1000);
        System.out.println("Pizza ordered!");
        this.pizzaOrdered = true;
        notifyAll();
        for(int i = 0; i <= 5 ; i++){
            Thread.sleep(1000);
            System.out.println(i * 20 + "% completion...");
        }
        System.out.println("Pizza is out for delivery");
    }

    public synchronized void deliverPizza() throws InterruptedException {
        while (!pizzaOrdered){
            wait();
        }
        Thread.sleep(4000);
        this.pizzaArrived = true;
        notifyAll();
    }

    public synchronized void checkStatus () throws InterruptedException {
        System.out.println("My stomach is " + this.status + " and I'm so hungry!!!");
        while(!status.equals("full")){
            wait();
        }
        System.out.println("I'm " + this.status + ", it's poopoo time!!!");
        Thread.sleep(3000);
        System.out.println("Alright done!!");
    }
}
