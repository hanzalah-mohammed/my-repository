package threadExample2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {

        Pizza refPizza = new Pizza();
        ExecutorService refExecutor = Executors.newCachedThreadPool();

        Runnable eatPizza = () -> {
            try {
                refPizza.eatPizza();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Runnable checkStatus = () -> {
            try {
                refPizza.checkStatus();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Runnable deliverPizza = () -> {
            try {
                refPizza.deliverPizza();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        };

        Runnable orderPizza = () -> {
            try {
                refPizza.orderPizza();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        // synchronized thread will run based on FIFO
        refExecutor.execute(checkStatus);
        refExecutor.execute(orderPizza);
        refExecutor.execute(eatPizza);
        refExecutor.execute(deliverPizza);
        refExecutor.shutdown();
    }
}
