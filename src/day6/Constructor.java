// Concept of constructor
// Why and when we need to use constructor?
// Differentiate method and a constructor
// Constructor is without public static void, and normally it's the same name with the Class.

package day6;

interface User2{
    default public void take(){

    }
}

class User {

    User() {
        System.out.println("The default constructor");
    }

    User(int number){ //default constructor
        System.out.println(number);
    }

    User(int number, String name){
        System.out.println(number + name);
    }
}

public class Constructor {
    public static void main(String[] args) {
        new User(10 , "Hannah");
//        new User(10);
//        new User("Hannah");
        new User();
    }
}
