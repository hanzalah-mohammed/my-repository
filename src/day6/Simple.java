package day6;

class Users {
    int number = 10;

    public void showNumber (){
        System.out.println(number);
    }

    void showName(String name){
        System.out.println(name);
    }

    void showStatus(boolean isGraduate){
        System.out.println(isGraduate);
    }

    void showAllDetails (int number, String name, boolean isGraduate){
        System.out.println(number);
        System.out.println(name);
        System.out.println(isGraduate);
    }
}

public class Simple {
    public static void main (String[] args) {
        Users uRef = new Users();
        uRef.showNumber();
        uRef.showName("Hannah");
        uRef.showStatus(false);

        uRef.showAllDetails(27, "John", true);
    }

}
