package day6;

public class Car {

    String colour = "green"; //default variable
    int model;               //default variable

    Car(String carColour, int modelNum) {
        colour = carColour;
        model =  modelNum;
    }

    public Car(String carColour) {
        colour = carColour;
    }

    public Car() {

    }

    public static void main(String[] args) {

        Car ferrari = new Car("red", 1234221);
        System.out.println(ferrari.colour);
        System.out.println(ferrari.model);

        Car honda = new Car("blue");
        System.out.println(honda.colour);

        Car toyota = new Car();
        System.out.println(toyota);
        System.out.println(toyota.colour);

    }
}
