package com.company;

public class Main { // contains more than 1 method with the same name within the same class

    public static void main(String[] args) { //start with this first main method
        main ("Hello from line 8"); // call the second method with the parameter
    }

    public static void main(String data) { //data holds the string from line 8
        main (10); // call the third method with the parameter
        System.out.println(data); //insert parameter from line 8
    }

    public static void main(int number) { //number holds the value 10 from line 13
        System.out.println(main("Hello from line 18", 500)); //call the line 21 and prints it
        System.out.println(number); //insert parameter from line 13
    }

    public static String main(String data, int number) {
        return data + " " + number;
    }

}
