package control.statement;

public class Switch {
    public static void main(String[] args) {

        String data = args[0];
        int condition = Integer.parseInt(data);

        switch (condition){
            case 0:
                System.out.println("Tokyo");
                break;
            case 1:
                System.out.println("Japan");
                break;
            case 2:
                System.out.println("Singapore");
                break;
            default:
                System.out.println("Condition not found");
        }
    }
}
