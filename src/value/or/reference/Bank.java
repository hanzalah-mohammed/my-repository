package value.or.reference;

import java.sql.SQLOutput;

public class Bank {

    // Variables
    String name;
    int balance = 1;

    // Constructor 1
    public Bank(String name) {
        this.name = name;
    }

    // Constructor 2
    public Bank(String name, int initialBalance) {
        this.name = name;
        balance = initialBalance;
    }

    @Override
    public String toString(){
        return "This is " + name + "'s bank account, and his balance is $" + balance + ".";
    }

    // Method 1
    public void checkBalance(){
        System.out.println("Balance: $" + balance + ".");
    }

    // Method 2
    public void deposit(int amountToDeposit){
        balance = balance + amountToDeposit;
        System.out.println(name + " has deposited $" + amountToDeposit + ", and now his balance is $" + balance + ".");
    }

    // Method 3
    public void withdraw(int amountToWithdraw){
        balance = balance - amountToWithdraw;
        System.out.println(name + " has withdrew $" + amountToWithdraw + ", and now his balance is $" + balance + ".");
    }


    // Main method
    public static void main(String[] args) {

        // John's Bank Account
        Bank johnAccount = new Bank("John", 2000);
        System.out.println(johnAccount);
//        johnAccount.deposit(500);
//        johnAccount.withdraw(1000);
//        johnAccount.checkBalance();
//
//        // James' Bank Account
        Bank JamesAccount = new Bank("James");
        System.out.println(JamesAccount);
//        JamesAccount.checkBalance();
    }
}
