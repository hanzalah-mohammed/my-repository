package value.or.reference;

public class Car {
    int modelNum;
    String brand = "Honda";

    public Car(){
        System.out.println("This is constructor 1");
    }

    public Car(int modelNum) {
        this();
        this.modelNum = modelNum;
        System.out.println("This is constructor 2");
    }

    public Car(int modelNum, String branding) {
        this(modelNum);
        brand = branding;
        System.out.println("This is constructor 3");
    }

    public static void main(String[] args) {
        Car toyota = new Car(123, "Toyota");
//        System.out.println(toyota.brand);
        Car honda = new Car(41241);
        Car unknown = new Car();
    }
}
