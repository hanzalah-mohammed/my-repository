package value.or.reference;

public class Droid {

    private String name = "hansom";
    private int batteryLevel;

    public Droid() {
        name = "Banzo";
        System.out.println("setting up");
    }

    public Droid(String droidName){
        name = droidName;
        batteryLevel = 100;
    }

    public String toString(){
        return "Hello, I'm the droid: " + name;
    }

    public void performTask(String task){
        System.out.println(name + " is performing task: " + task);
        batteryLevel = batteryLevel - 10;
    }

    public void checkBatteryLevel(){
        System.out.println(name + "'s battery level is " + batteryLevel + "%");
    }

    public static void main (String[] args){

//        Droid codey = new Droid("Codey");
//        System.out.println(codey);
//        codey.performTask("clean the room");
//        codey.checkBatteryLevel();

        Droid refDroid = new Droid();
        System.out.println(refDroid.name);
        System.out.println(refDroid.batteryLevel);

    }
}
