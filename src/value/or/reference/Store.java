package value.or.reference;

public class Store {

    String shopName;
    String product;
    double price;

    public Store(String shopName, String product, int price) {
        this.shopName = shopName;
        this.product = product;
        this.price = price;
    }

    public void showDetails(){
        System.out.println("The " + shopName + "shop sells " + product + " with the price of " + price + ".");
    }

    public double checkPriceWithTax(){
        double tax = 0.08;
        double totalPrice = price + (price * tax);
        return totalPrice;
    }

    public static void main(String[] args) {
        Store sallyStore = new Store("Sally cookie", "cookie", 3);
        sallyStore.showDetails();
        System.out.println("The total price with tax is $" + sallyStore.checkPriceWithTax());
        sallyStore.showDetails();
    }
}
