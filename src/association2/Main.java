package association2;

public class Main {
    public static void main(String[] args) {
        Student refStudent = new Student("James");
        Department refDepartment = new Department(refStudent);
        University refUniversity = new University(refDepartment);

        System.out.println(refUniversity);
    }
}
