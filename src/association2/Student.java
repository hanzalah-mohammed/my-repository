package association2;

public class Student {
    private String studentName;

    public Student(String studentName) {
        this.studentName = studentName;
    }

    @Override
    public String toString() {
        return studentName;
    }
}
