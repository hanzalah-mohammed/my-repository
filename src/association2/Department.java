package association2;

public class Department {
    Student refStudent;

    public Department(Student refStudent) {
        this.refStudent = refStudent;
    }

    @Override
    public String toString() {
        return refStudent.toString();
    }
}
