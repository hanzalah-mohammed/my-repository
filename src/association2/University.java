package association2;

public class University {

    Department refDepartment;

    public University(Department refDepartment) {
        this.refDepartment = refDepartment;
    }

    @Override
    public String toString() {
        return refDepartment.toString();
    }
}
