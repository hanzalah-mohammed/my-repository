package package2;

import static java.lang.System.*;
import static java.lang.Math.*;

public class Static2 {

    public static void print(){
        out.println("method from another package and class");
    }

    public static void plus(int a){
        double randomNumber = random();
        int absoluteNumber = abs(a);
        out.println(randomNumber);
        out.println(absoluteNumber);
    }
}
