package concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Example7 {

    public static void main(String[] args) {

        Runnable thread1 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread ID " + Thread.currentThread().getName());
            }
        };

        Runnable thread2 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread ID " + Thread.currentThread().getName());
            }
        };

        Runnable thread3 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread ID " + Thread.currentThread().getName());
            }
        };

        Runnable thread4 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread ID " + Thread.currentThread().getName());
            }
        };

        ExecutorService refExecutor = Executors.newFixedThreadPool(4);
        ScheduledExecutorService refScheduledExecutor = Executors.newScheduledThreadPool(3);

        refExecutor.submit(thread1);
        refExecutor.submit(thread2);
        refExecutor.submit(thread3);
        refExecutor.submit(thread4);
        refScheduledExecutor.schedule(thread1, 5000, TimeUnit.MILLISECONDS);

        refExecutor.shutdown();
    }



}
