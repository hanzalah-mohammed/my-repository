package concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Example4 {

    public static void main(String[] args) {

        Runnable thread1 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread 1 " + Thread.currentThread().getName());
            }
        };

        Runnable thread2 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread 2 " + Thread.currentThread().getName());
            }
        };

        Runnable thread3 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread 3 " + Thread.currentThread().getName());
            }
        };

        Runnable thread4 = () -> {
            for(int i = 0; i < 3; i++){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Loop " + (i+1) + ": " + "Thread 4 " + Thread.currentThread().getName());
            }
        };

//        ExecutorService refExecutor = Executors.newSingleThreadExecutor();
        ExecutorService refExecutor = Executors.newFixedThreadPool(2);
//        ExecutorService refExecutor = Executors.newCachedThreadPool();
//        ScheduledExecutorService refScheduledExecutor = Executors.newScheduledThreadPool(3);

        refExecutor.submit(thread1);
        refExecutor.submit(thread3);
        refExecutor.submit(thread2);
        refExecutor.submit(thread4);
//        refScheduledExecutor.schedule(thread1, 5000, TimeUnit.MILLISECONDS);

        refExecutor.shutdown();
    }
    
}
