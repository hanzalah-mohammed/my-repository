package concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Example2 {

    public static void main(String[] args) {
        ExecutorService refExecutorService = Executors.newSingleThreadExecutor();

        // A Callable task returns a value upon completion and we use the Future object to obtain the value

        Callable<Integer> refCallable = () -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            return 1000;
        };
        Future<Integer> result = refExecutorService.submit(refCallable);
        try {
            Integer returnValue = result.get(); // blocks the current thread until the task completes and returns the value.
            System.out.println("Return value = " + returnValue);    // output after 5 seconds
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        refExecutorService.shutdown();
    }
}
